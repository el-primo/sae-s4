var searchData=
[
  ['s_20changed_20since_20moving_20from_20sourceforge_0',['What&apos;s changed since moving from SourceForge?',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md23',1,'']]],
  ['saphir_1',['BIENVENU AU GROUPE SAPHIR',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_r_e_a_d_m_e.html#autotoc_md1',1,'']]],
  ['secureheader_2',['secureHeader',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a808495366277050ff38fa5bcfab8f49e',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['security_3',['Security',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md17',1,'']]],
  ['security_20notices_20relating_20to_20phpmailer_4',['Security notices relating to PHPMailer',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_s_e_c_u_r_i_t_y.html',1,'']]],
  ['send_5',['send',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a12bcef5130168b80d3d52dc82213f19a',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['sendandmail_6',['sendAndMail',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#a06bd4d1e4270cbabec07624d194bacfb',1,'PHPMailer::PHPMailer::SMTP']]],
  ['sendcommand_7',['sendCommand',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#ad277f27506e6e9cdea6073ffa5f7b476',1,'PHPMailer::PHPMailer::SMTP']]],
  ['sendhello_8',['sendHello',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#a2bbc3656b8c44e29ee5621ee7afc891f',1,'PHPMailer::PHPMailer::SMTP']]],
  ['sendmailsend_9',['sendmailSend',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a05940f686d59bcc249d8d42a6f2e7df0',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['sendstring_10',['sendString',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_o_p3.html#a517133fc963d79977f1eae330e14e0d1',1,'PHPMailer::PHPMailer::POP3']]],
  ['serverhostname_11',['serverHostname',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#ae0867f9651bd0b2f78fb7ccca41377a2',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['set_12',['set',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a71e1bd499bee8845b64d3a41f893cc3d',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['setboundaries_13',['setBoundaries',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a630c735be0fc9e91719454cd95a50e68',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['setdebuglevel_14',['setDebugLevel',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#aa77813947ee5266b5e31839356eed03c',1,'PHPMailer::PHPMailer::SMTP']]],
  ['setdebugoutput_15',['setDebugOutput',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#a2a086f3a7e0f0aab597c48c40371ef81',1,'PHPMailer::PHPMailer::SMTP']]],
  ['seterror_16',['setError',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a9b84595d8561a0abd81c0b83b6238472',1,'PHPMailer\PHPMailer\PHPMailer\setError()'],['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_o_p3.html#a0bc8cac7e04a83c9ca87a99690ef1194',1,'PHPMailer\PHPMailer\POP3\setError()'],['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#ab7cbdee885b82ec97d12356f551b6d10',1,'PHPMailer\PHPMailer\SMTP\setError()']]],
  ['setfrom_17',['setFrom',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#afdcdc81498cd812c6357df6783e35932',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['setlanguage_18',['setLanguage',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#aaf95e152019d45c4721612c4a08dbcd9',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['setle_19',['setLE',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a19fbf095d30e95a9ee7e448e7656653c',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['setmessagetype_20',['setMessageType',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a333e5e1e8f7aabf320d5f2f950812d5b',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['setoauth_21',['setOAuth',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a4e8ca9b8795678d108ff29d953f95039',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['setsmtpinstance_22',['setSMTPInstance',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#ab7be80328641ccf5f4e2d8f17a2ea99b',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['setsmtpxclientattribute_23',['setSMTPXclientAttribute',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a8b7cb8c10be33eae3f5b734647f1a759',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['settimeout_24',['setTimeout',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#aa359a3fe4a000a856cc9cb7b8b92cb29',1,'PHPMailer::PHPMailer::SMTP']]],
  ['setverp_25',['setVerp',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#a450b8d7a4dd1749a75f5dd0c48143f6f',1,'PHPMailer::PHPMailer::SMTP']]],
  ['setwordwrap_26',['setWordWrap',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#ab3b2787c3d322ed943f863c6bf2e4bc8',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['sign_27',['sign',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#ade9654aa4a545955b7058c18f837db8f',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['simple_20example_28',['A Simple Example',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md13',1,'']]],
  ['since_20moving_20from_20sourceforge_29',['What&apos;s changed since moving from SourceForge?',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md23',1,'']]],
  ['smtp_30',['SMTP',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html',1,'PHPMailer::PHPMailer']]],
  ['smtpclose_31',['smtpClose',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a3ec2fab4c4b1ab20a992704c35be6364',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['smtpconnect_32',['smtpConnect',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a58e7e9d1a3e59fc691476b400a7b8fe0',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['smtpsend_33',['smtpSend',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a49eca48203f71281e82986420773c3be',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['sourceforge_34',['What&apos;s changed since moving from SourceForge?',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md23',1,'']]],
  ['sponsorship_35',['Sponsorship',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md19',1,'']]],
  ['startdiscussion_36',['startDiscussion',['../class_model.html#a7400f24e2edd3aaef1da2ee8a767f53a',1,'Model']]],
  ['starttls_37',['startTLS',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#a58d9443a0a1690565edae142b85e0e26',1,'PHPMailer::PHPMailer::SMTP']]],
  ['striptrailingbreaks_38',['stripTrailingBreaks',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a7f6bad24ee784ac57e6e2241212f1398',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['striptrailingwsp_39',['stripTrailingWSP',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a26b2c13f3afc9f3a947c5134ace7cf96',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['support_40',['OAuth2 Support',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md30',1,'']]]
];
