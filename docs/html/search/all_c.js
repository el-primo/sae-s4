var searchData=
[
  ['featured_20email_20creation_20and_20transfer_20class_20for_20php_0',['PHPMailer – A full-featured email creation and transfer class for PHP',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md5',1,'']]],
  ['features_1',['Features',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md6',1,'']]],
  ['fileisaccessible_2',['fileIsAccessible',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#aab8c2ed97c28e364036dc95f42547397',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['filenametotype_3',['filenameToType',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#ac1a4d9403511e262d4723394052bf015',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['for_20enterprise_4',['PHPMailer For Enterprise',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md20',1,'']]],
  ['for_20php_5',['PHPMailer – A full-featured email creation and transfer class for PHP',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md5',1,'']]],
  ['from_205_202_6',['Upgrading from 5.2',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md11',1,'']]],
  ['from_20phpmailer_205_202_20to_206_200_7',['Upgrading from PHPMailer 5.2 to 6.0',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html',1,'']]],
  ['from_20sourceforge_8',['What&apos;s changed since moving from SourceForge?',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md23',1,'']]],
  ['full_20featured_20email_20creation_20and_20transfer_20class_20for_20php_9',['PHPMailer – A full-featured email creation and transfer class for PHP',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md5',1,'']]],
  ['fws_10',['FWS',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#ae3e1ad830c3d45920d117d58e9dcf735',1,'PHPMailer::PHPMailer::PHPMailer']]]
];
