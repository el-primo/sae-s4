var searchData=
[
  ['parseaddresses_0',['parseAddresses',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#aadeeafdb5f9ba571b01ed3dc58afb236',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['parsehellofields_1',['parseHelloFields',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html#a526777ee97d964128c3c8c235f318437',1,'PHPMailer::PHPMailer::SMTP']]],
  ['parseurl_2',['parseUrl',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_d_s_n_configurator.html#a6147fe97c0be7e217e24dc2ec97fda27',1,'PHPMailer::PHPMailer::DSNConfigurator']]],
  ['perform_5fvision_3',['Perform_Vision',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_r_e_a_d_m_e.html',1,'']]],
  ['php_4',['PHPMailer – A full-featured email creation and transfer class for PHP',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md5',1,'']]],
  ['php_20version_5',['PHP Version',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md26',1,'']]],
  ['phpmailer_6',['PHPMailer',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md27',1,'Loading PHPMailer'],['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html',1,'PHPMailer'],['../namespace_p_h_p_mailer.html',1,'PHPMailer'],['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_s_e_c_u_r_i_t_y.html',1,'Security notices relating to PHPMailer']]],
  ['phpmailer_20–_20a_20full_20featured_20email_20creation_20and_20transfer_20class_20for_20php_7',['PHPMailer – A full-featured email creation and transfer class for PHP',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md5',1,'']]],
  ['phpmailer_205_202_20to_206_200_8',['Upgrading from PHPMailer 5.2 to 6.0',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html',1,'']]],
  ['phpmailer_20for_20enterprise_9',['PHPMailer For Enterprise',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md20',1,'']]],
  ['phpmailer_3a_3aphpmailer_10',['PHPMailer',['../namespace_p_h_p_mailer_1_1_p_h_p_mailer.html',1,'PHPMailer']]],
  ['pop3_11',['POP3',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_o_p3.html',1,'PHPMailer::PHPMailer']]],
  ['popbeforesmtp_12',['popBeforeSmtp',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_o_p3.html#a34779155a9c8d84b2e01aecc928f7d30',1,'PHPMailer::PHPMailer::POP3']]],
  ['postsend_13',['postSend',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#aa2e32c2514a2f342c87509f9d9af34cb',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['presend_14',['preSend',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a528cfdd34d79b053a812a735632593ea',1,'PHPMailer::PHPMailer::PHPMailer']]],
  ['projet_20_3a_15',['DESCRIPTION DU PROJET :',['../md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_r_e_a_d_m_e.html#autotoc_md3',1,'']]],
  ['punyencodeaddress_16',['punyencodeAddress',['../class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a53f031a9823e893efcae14422999619b',1,'PHPMailer::PHPMailer::PHPMailer']]]
];
