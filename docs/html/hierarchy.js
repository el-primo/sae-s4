var hierarchy =
[
    [ "Controller", "class_controller.html", [
      [ "Controller_about", "class_controller__about.html", null ],
      [ "Controller_activity", "class_controller__activity.html", null ],
      [ "Controller_auth", "class_controller__auth.html", null ],
      [ "Controller_dashboard", "class_controller__dashboard.html", null ],
      [ "Controller_discussion", "class_controller__discussion.html", null ],
      [ "Controller_formateurs", "class_controller__formateurs.html", null ],
      [ "Controller_home", "class_controller__home.html", null ],
      [ "Controller_information", "class_controller__information.html", null ],
      [ "Controller_panel", "class_controller__panel.html", null ],
      [ "Controller_parametre", "class_controller__parametre.html", null ],
      [ "Controller_profile", "class_controller__profile.html", null ]
    ] ],
    [ "DSNConfigurator", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_d_s_n_configurator.html", null ],
    [ "EmailSender", "class_email_sender.html", null ],
    [ "Exception", null, [
      [ "Exception", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_exception.html", null ]
    ] ],
    [ "Model", "class_model.html", null ],
    [ "OAuthTokenProvider", "interface_p_h_p_mailer_1_1_p_h_p_mailer_1_1_o_auth_token_provider.html", [
      [ "OAuth", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_o_auth.html", null ]
    ] ],
    [ "PHPMailer", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html", null ],
    [ "POP3", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_o_p3.html", null ],
    [ "SMTP", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html", null ]
];