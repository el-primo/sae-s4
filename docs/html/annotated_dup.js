var annotated_dup =
[
    [ "PHPMailer", "namespace_p_h_p_mailer.html", [
      [ "PHPMailer", "namespace_p_h_p_mailer_1_1_p_h_p_mailer.html", [
        [ "DSNConfigurator", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_d_s_n_configurator.html", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_d_s_n_configurator" ],
        [ "Exception", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_exception.html", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_exception" ],
        [ "OAuth", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_o_auth.html", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_o_auth" ],
        [ "OAuthTokenProvider", "interface_p_h_p_mailer_1_1_p_h_p_mailer_1_1_o_auth_token_provider.html", "interface_p_h_p_mailer_1_1_p_h_p_mailer_1_1_o_auth_token_provider" ],
        [ "PHPMailer", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer" ],
        [ "POP3", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_o_p3.html", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_o_p3" ],
        [ "SMTP", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p.html", "class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_s_m_t_p" ]
      ] ]
    ] ],
    [ "Controller", "class_controller.html", "class_controller" ],
    [ "Controller_about", "class_controller__about.html", "class_controller__about" ],
    [ "Controller_activity", "class_controller__activity.html", "class_controller__activity" ],
    [ "Controller_auth", "class_controller__auth.html", "class_controller__auth" ],
    [ "Controller_dashboard", "class_controller__dashboard.html", "class_controller__dashboard" ],
    [ "Controller_discussion", "class_controller__discussion.html", "class_controller__discussion" ],
    [ "Controller_formateurs", "class_controller__formateurs.html", "class_controller__formateurs" ],
    [ "Controller_home", "class_controller__home.html", "class_controller__home" ],
    [ "Controller_information", "class_controller__information.html", "class_controller__information" ],
    [ "Controller_panel", "class_controller__panel.html", "class_controller__panel" ],
    [ "Controller_parametre", "class_controller__parametre.html", "class_controller__parametre" ],
    [ "Controller_profile", "class_controller__profile.html", "class_controller__profile" ],
    [ "EmailSender", "class_email_sender.html", null ],
    [ "Model", "class_model.html", "class_model" ]
];