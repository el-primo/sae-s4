/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "SAE S4", "index.html", [
    [ "Perform_Vision", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_r_e_a_d_m_e.html", [
      [ "BIENVENU AU GROUPE SAPHIR", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_r_e_a_d_m_e.html#autotoc_md1", [
        [ "INSTALLATION :", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_r_e_a_d_m_e.html#autotoc_md2", null ],
        [ "DESCRIPTION DU PROJET :", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_r_e_a_d_m_e.html#autotoc_md3", null ]
      ] ],
      [ "les mdp des comptes :", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_r_e_a_d_m_e.html#autotoc_md4", null ]
    ] ],
    [ "README", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html", [
      [ "PHPMailer – A full-featured email creation and transfer class for PHP", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md5", [
        [ "Features", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md6", null ],
        [ "Why you might need it", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md7", null ],
        [ "License", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md8", null ],
        [ "Installation & loading", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md9", null ],
        [ "Legacy versions", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md10", [
          [ "Upgrading from 5.2", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md11", null ],
          [ "Minimal installation", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md12", null ]
        ] ],
        [ "A Simple Example", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md13", null ],
        [ "Localization", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md14", null ],
        [ "Documentation", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md15", null ],
        [ "Tests", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md16", null ],
        [ "Security", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md17", null ],
        [ "Contributing", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md18", null ],
        [ "Sponsorship", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md19", null ],
        [ "PHPMailer For Enterprise", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md20", null ],
        [ "Changelog", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md21", null ],
        [ "History", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md22", [
          [ "What's changed since moving from SourceForge?", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_r_e_a_d_m_e.html#autotoc_md23", null ]
        ] ]
      ] ]
    ] ],
    [ "Security notices relating to PHPMailer", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_s_e_c_u_r_i_t_y.html", null ],
    [ "Upgrading from PHPMailer 5.2 to 6.0", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html", [
      [ "PHP Version", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md26", null ],
      [ "Loading PHPMailer", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md27", null ],
      [ "Namespace", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md28", null ],
      [ "Namespaced exceptions", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md29", null ],
      [ "OAuth2 Support", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md30", null ],
      [ "Extras", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md31", null ],
      [ "Other upgrade changes", "md__c_1_2_users_2aln_2_desktop_2workspace_2paqueta_2_utils_2_p_h_p_mailer_2_u_p_g_r_a_d_i_n_g.html#autotoc_md32", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"_list-_unsubscribe-example.html",
"class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a8b7cb8c10be33eae3f5b734647f1a759",
"functions_v.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';