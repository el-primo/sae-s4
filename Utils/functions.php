<?php

/**
 * Fonction pour assainir les entrées utilisateur.
 * 
 * Cette fonction supprime les espaces indésirables et convertit les caractères spéciaux en entités HTML.
 * 
 * @param string $input Entrée utilisateur à assainir.
 * @return string Entrée assainie.
 */
function sanitizeInput($input)
{
    return htmlspecialchars(trim($input), ENT_QUOTES, 'UTF-8');
}

/**
 * Valide les entrées de connexion.
 * 
 * @param string $email L'email de l'utilisateur.
 * @param string $password Le mot de passe de l'utilisateur.
 * @return bool Retourne true si les entrées sont valides, sinon false.
 */
function validateLoginInput($email, $password)
{
    return filter_var($email, FILTER_VALIDATE_EMAIL) && !empty($password) && strlen($password) <= 256 && strlen($email) <= 128;
}

/**
 * Échappe les caractères spéciaux dans une chaîne de caractères.
 * 
 * Cette fonction est utilisée pour échapper les caractères spéciaux HTML dans une chaîne de caractères.
 * 
 * @param string $message La chaîne à échapper.
 * @return string Chaîne échappée.
 */
function e($message)
{
    return htmlspecialchars($message, ENT_QUOTES);
}

/**
 * Vérifie si l'utilisateur a accès aux fonctionnalités.
 * 
 * Cette fonction vérifie si l'utilisateur est connecté et si sa session est valide.
 * 
 * @return mixed Renvoie les détails de l'utilisateur si la session est valide, sinon false.
 */
function checkUserAccess()
{
    if (!isset($_SESSION['user_id']) || !isset($_SESSION['user_token']) || $_SESSION['expire_time'] < time()) {
        return false;
    }

    $user = Model::getModel()->verifierToken(e($_SESSION['user_token']));

    if (!$user) {
        return false;
    }

    return $user;
}

/**
 * Récupère le rôle de l'utilisateur.
 * 
 * Cette fonction récupère le rôle de l'utilisateur (client ou formateur) à partir de son identifiant.
 * 
 * @param array $user Les détails de l'utilisateur.
 * @return string Le rôle de l'utilisateur (Client ou Formateur).
 */
function getUserRole($user)
{
    $formateurDetails = Model::getModel()->getFormateurById(e($user['id_utilisateur']));
    return ($formateurDetails) ? 'Formateur' : 'Client';
}

/**
 * Vérifie si l'utilisateur est dans une discussion spécifique.
 * 
 * Cette fonction vérifie si l'utilisateur est participant à une discussion en particulier.
 * 
 * @param int $userId L'identifiant de l'utilisateur.
 * @param array $discussion Les détails de la discussion.
 * @return bool Renvoie true si l'utilisateur est dans la discussion, sinon false.
 */
function isUserInDiscussion($userId, $discussion)
{
    return $userId == $discussion['id_utilisateur'] || $userId == $discussion['id_utilisateur_1'];
}

/**
 * Fonction pour chiffrer une chaîne de caractères avec une clé publique RSA.
 * 
 * @param string $str La chaîne à chiffrer.
 * @return string La chaîne chiffrée en base64.
 */
function encryptWithPublicKey($str)
{
    $publicKey = openssl_pkey_get_public(file_get_contents("key.public"));
    openssl_public_encrypt($str, $crypted, $publicKey);
    return base64_encode($crypted);
}

/**
 * Fonction pour déchiffrer une chaîne de caractères avec une clé privée RSA.
 * 
 * @param string $str La chaîne à déchiffrer en base64.
 * @return string La chaîne déchiffrée.
 */
function decryptWithPrivateKey($str)
{
    $privateKey = openssl_pkey_get_private(file_get_contents("key.private"));
    openssl_private_decrypt(base64_decode($str), $decrypted, $privateKey);
    return $decrypted;
}

/**
 * Génère une paire de clés RSA.
 * 
 * Cette fonction génère une paire de clés RSA (publique et privée) avec une longueur de bits spécifiée.
 * 
 * @param int $bitLength La longueur en bits des clés RSA.
 * @return array Un tableau contenant la clé publique et la clé privée.
 */
function generateRSAKeys($bitLength)
{
    $p = generatePrime($bitLength);
    $q = generatePrime($bitLength);

    $n = gmp_mul($p, $q);
    $phi = gmp_mul(gmp_sub($p, 1), gmp_sub($q, 1));

    $e = findCoprime($phi);
    $d = modInverse($e, $phi);

    return [
        'publicKey' => ['e' => gmp_strval($e), 'n' => gmp_strval($n)],
        'privateKey' => ['d' => gmp_strval($d), 'n' => gmp_strval($n)]
    ];
}

/**
 * Génère un nombre premier de la longueur spécifiée en bits.
 * 
 * @param int $bitLength La longueur en bits du nombre premier.
 * @return resource Le nombre premier généré.
 */
function generatePrime($bitLength)
{
    do {
        $randomNumber = gmp_random_bits($bitLength);
    } while (!gmp_prob_prime($randomNumber, 50));

    return $randomNumber;
}

/**
 * Trouve un nombre qui est copremier avec phi.
 * 
 * @param resource $phi La valeur de phi.
 * @return resource Le nombre copremier.
 */
function findCoprime($phi)
{
    $e = gmp_init(65537);
    $phi = gmp_init($phi);

    while (gmp_cmp(gmp_gcd($e, $phi), 1) != 0) {
        $e = gmp_add($e, 1);
    }

    return $e;
}

/**
 * Calcule l'inverse modulaire de a modulo m.
 * 
 * @param resource $a La valeur a.
 * @param resource $m La valeur m.
 * @return resource L'inverse modulaire.
 */
function modInverse($a, $m)
{
    $a = gmp_init($a);
    $m = gmp_init($m);

    $inv = gmp_invert($a, $m);

    return $inv;
}

/**
 * Calcule (base^exposant) % modulo en utilisant l'exponentiation modulaire.
 * 
 * @param resource $base La base.
 * @param resource $exposant L'exposant.
 * @param resource $modulo Le modulo.
 * @return string Le résultat de (base^exposant) % modulo.
 */
function modPow($base, $exposant, $modulo)
{
    $base = gmp_init($base);
    $exposant = gmp_init($exposant);
    $modulo = gmp_init($modulo);

    $resultatFinal = gmp_init(1);

    while (gmp_cmp($exposant, 0) > 0) {

        if (gmp_cmp(gmp_mod($exposant, 2), 1) == 0) {
            $resultatFinal = gmp_mod(gmp_mul($resultatFinal, $base), $modulo);
        }

        $base = gmp_mod(gmp_mul($base, $base), $modulo);

        $exposant = gmp_div($exposant, 2);
    }

    return gmp_strval($resultatFinal);
}

/**
 * Convertit une chaîne de caractères en un nombre.
 * 
 * @param string $string La chaîne de caractères à convertir.
 * @return resource Le nombre converti.
 */
function stringToNumber($string)
{
    $result = gmp_init('0');
    $length = strlen($string);

    for ($i = 0; i < $length; $i++) {
        $result = gmp_mul($result, '256');
        $result = gmp_add($result, ord($string[$i]));
    }

    return $result;
}

/**
 * Convertit un nombre en une chaîne de caractères.
 * 
 * @param resource $number Le nombre à convertir.
 * @return string La chaîne de caractères convertie.
 */
function numberToString($number)
{
    $result = '';

    while (gmp_cmp($number, 0) > 0) {
        $byte = gmp_mod($number, '256');
        $result = chr((int) gmp_strval($byte)) . $result;
        $number = gmp_div($number, '256', GMP_ROUND_ZERO);
    }

    return $result;
}

/**
 * Chiffre un message en utilisant la clé publique RSA.
 * 
 * @param string $message Le message à chiffrer.
 * @param array $publicKey La clé publique.
 * @return string Le message chiffré.
 */
function encryptRSA($message, $publicKey)
{
    $numericMessage = stringToNumber($message);
    $encryptedMessage = modPow($numericMessage, $publicKey['e'], $publicKey['n']);
    return $encryptedMessage;
}

/**
 * Déchiffre un message en utilisant la clé privée RSA.
 * 
 * @param string $encryptedMessage Le message chiffré.
 * @param array $privateKey La clé privée.
 * @return string Le message déchiffré.
 */
function decryptRSA($encryptedMessage, $privateKey)
{
    $decryptedNumericMessage = modPow($encryptedMessage, $privateKey['d'], $privateKey['n']);
    $decryptedMessage = numberToString($decryptedNumericMessage);
    return $decryptedMessage;
}


function genererCodeAleatoire($longueur) {
    // Définir les caractères possibles pour le code
    $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $longueurCaracteres = strlen($caracteres);
    $codeAleatoire = '';

    // Générer le code
    for ($i = 0; $i < $longueur; $i++) {
        $codeAleatoire .= $caracteres[rand(0, $longueurCaracteres - 1)];
    }

    return $codeAleatoire;
}

/*$keys = generateRSAKeys(1024);
$publicKey = $keys['publicKey'];
$privateKey = $keys['privateKey'];

$message = "hello, je suis trop fort";

$encryptedMessage = encryptRSA($message, $publicKey);
echo "Message chiffré : " . $encryptedMessage . PHP_EOL;

$decryptedMessage = decryptRSA($encryptedMessage, $privateKey);
echo "Message déchiffré : " . $decryptedMessage . PHP_EOL;
*/