const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

// Vérifiez si les éléments existent avant d'ajouter des écouteurs d'événements
if (signUpButton && signInButton && container) {
    signUpButton.addEventListener('click', () => {
        container.classList.add("right-panel-active");
    });

    signInButton.addEventListener('click', () => {
        container.classList.remove("right-panel-active");
    });
}

document.getElementById('loginform').addEventListener('submit', (event)=> {
            event.preventDefault();

            var formData = new FormData(this);

            fetch('?controller=auth&action=login', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                if (data.status === 'success') {
                    alert('Connexion réussie!');
                    // Redirection vers le tableau de bord après un court délai
                    setTimeout(() => {
                        window.location.href = '?controller=dashboard';
                    }, 1000);
                } else {
                    alert(data.message);
                }
            })
            .catch(error => console.error('Erreur:', error));
});
        
document.getElementById('registrationForm').addEventListener('submit', function(event) {
    event.preventDefault();

    var formData = new FormData(this);

    fetch('?controller=auth&action=register_ajax', {
        method: 'POST',
        body: formData
    })
    .then(async response => {
        // Check if the response is JSON
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.indexOf('application/json') !== -1) {
            return response.json();
        } else {
            // Handle non-JSON response
            const text = await response.text();
            throw new Error(text);
        }
    })
    .then(data => {
        if (data.status == 'success') {
            alert('Inscription réussie! Un e-mail de vérification a été envoyé.');
            window.location.href = '?controller=dashboard';
        } else {
            alert(data.message);
        }
    })
    .catch(error => {
        console.error('Erreur:', error);
        alert('Une erreur est survenue. Veuillez réessayer.');
    });
});
