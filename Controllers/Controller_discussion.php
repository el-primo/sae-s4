<?php

class Controller_discussion extends Controller
{    /**
     * Action par défaut.
     *
     * Redirige vers l'action "action_list".
     */
    public function action_default()
    {
        $this->action_list();
    }


    /**
     * Affiche la liste des discussions.
     *
     * Vérifie d'abord l'accès de l'utilisateur.
     * Récupère le rôle de l'utilisateur.
     * Récupère les discussions de l'utilisateur à partir du modèle.
     * Construit une liste de discussions avec les détails nécessaires.
     * Envoie les données à la vue 'discussion_list' pour affichage.
     */
    public function action_list()
    {
        // Vérifie l'accès de l'utilisateur
        $user = checkUserAccess();
        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
        }

        // Récupère le rôle de l'utilisateur
        $role = getUserRole($user);

        // Récupère le modèle
        $model = Model::getModel();

        // Récupère les discussions de l'utilisateur
        $discussions = $model->recupererDiscussion($user['id_utilisateur']);

        // Initialise une liste vide pour les discussions
        $discussionList = [];

        // Parcourt chaque discussion pour récupérer les détails nécessaires
        foreach ($discussions as $discussion) {
            $interlocuteurId = ($role === 'Client') ? $discussion['id_utilisateur_1'] : $discussion['id_utilisateur'];
            $interlocuteur = $model->getUserById($interlocuteurId);

            // Vérifie si l'interlocuteur existe
            if (!$interlocuteur) {
                continue;
            }

            // Compte les messages non lus dans la discussion
            $unreadMessages = $model->countUnreadMessages($interlocuteurId, $discussion['id_discussion']);

            // Ajoute les détails de la discussion à la liste
            $discussionList[] = [
                'discussion_id' => $discussion['id_discussion'],
                'nom_interlocuteur' => $interlocuteur['nom'],
                'prenom_interlocuteur' => $interlocuteur['prenom'],
                'photo_interlocuteur' => $interlocuteur['photo_de_profil'],
                'unread_messages' => ($unreadMessages > 0),
            ];
        }

        // Prépare les données à envoyer à la vue
        $data = [
            'nom' => $user['nom'],
            'prenom' => $user['prenom'],
            'photo_de_profil' => $user['photo_de_profil'],
            'role' => $role,
            'discussions' => $discussionList
        ];

        // Affiche la vue 'discussion_list' avec les données fournies
        $this->render('discussion_list', $data);
    }


    /**
     * Affiche une discussion spécifique.
     *
     * Vérifie d'abord l'accès de l'utilisateur.
     * Récupère l'ID de la discussion à partir de la requête GET.
     * Vérifie si l'utilisateur est modérateur ou fait partie de la discussion.
     * Récupère les détails de la discussion et de son destinataire.
     * Récupère les messages de la discussion.
     * Affiche la discussion avec les données récupérées.
     */
    public function action_discussion()
    {
        // Vérifie l'accès de l'utilisateur
        $user = checkUserAccess();
        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
        }

        // Récupère le modèle
        $model = Model::getModel();

        // Récupère l'ID de la discussion depuis la requête GET
        $discussionId = isset($_GET['id']) ? e($_GET['id']) : null;

        // Redirige si l'ID de la discussion n'est pas fourni
        if (!$discussionId) {
            header('Location: ?controller=discussion');
            exit();
        }

        // Vérifie si l'utilisateur est modérateur
        $isModo = $model->verifModerateur($user['id_utilisateur']);

        // Récupère les détails de la discussion
        $discussion = $model->getDiscussionById($discussionId);

        // Redirige si la discussion n'existe pas ou si l'utilisateur n'y participe pas
        if (!$discussion || !($isModo || isUserInDiscussion($user['id_utilisateur'], $discussion))) {
            header('Location: ?controller=discussion');
            exit();
        }

        // Récupère le rôle de l'utilisateur
        $role = getUserRole($user);

        // Détermine l'ID du destinataire en fonction du rôle
        $receiverId = ($role === 'Client') ? $discussion['id_utilisateur_1'] : $discussion['id_utilisateur'];

        // Récupère les détails du destinataire
        $receiver = $model->getUserById($receiverId);

        // Redirige si le destinataire n'existe pas
        if (!$receiver) {
            header('Location: ?controller=discussion');
            exit();
        }

        // Récupère les messages de la discussion
        $messages = $model->messagesDiscussion($discussionId);

        // Prépare les données à envoyer à la vue
        $data = [
            'nom_receiver' => $receiver['nom'],
            'prenom_receiver' => $receiver['prenom'],
            'photo_receiver' => $receiver['photo_de_profil'],
            'messages' => $messages,
            'nom' => $user['nom'],
            'prenom' => $user['prenom'],
            'photo_de_profil' => $user['photo_de_profil'],
            'role' => $role,
            'user_id' => $user['id_utilisateur'],
            'isModo' => $isModo
        ];

        // Affiche la vue 'discussion' avec les données fournies
        $this->render('discussion', $data);
    }

    /**
     * Envoie un message dans une discussion.
     *
     * Vérifie si la méthode de requête est POST.
     * Vérifie l'accès de l'utilisateur.
     * Récupère l'ID de la discussion à partir des données POST.
     * Vérifie si l'utilisateur fait partie de la discussion.
     * Récupère le texte du message et détermine s'il doit être validé par un modérateur.
     * Ajoute le message à la discussion.
     * Redirige vers la discussion après l'envoi du message.
     */
    public function action_envoi_message()
    {
        // Vérifie si la méthode de requête est POST
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            header('Location: ?controller=discussion');
            exit();
        }

        // Vérifie l'accès de l'utilisateur
        $user = checkUserAccess();
        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
        }

        // Récupère le modèle
        $model = Model::getModel();

        // Récupère l'ID de la discussion à partir des données POST
        $discussionId = isset($_POST['discussionId']) ? e($_POST['discussionId']) : null;

        // Redirige si l'ID de la discussion n'est pas fourni
        if (!$discussionId) {
            header('Location: ?controller=discussion');
            exit();
        }

        // Récupère les détails de la discussion
        $discussion = $model->getDiscussionById($discussionId);

        // Redirige si la discussion n'existe pas ou si l'utilisateur n'y participe pas
        if (!$discussion || !isUserInDiscussion($user['id_utilisateur'], $discussion)) {
            header('Location: ?controller=discussion');
            exit();
        }

        // Récupère le texte du message
        $texteMessage = isset($_POST['texte_message']) ? e($_POST['texte_message']) : '';

        // Vérifie si l'utilisateur est admin, modérateur ou affranchi
        $isAdmin = $model->verifAdmin($user['id_utilisateur']);
        $isModo = $model->verifModerateur($user['id_utilisateur']);
        $isAffranchi = $model->verifAffranchiModerateur($user['id_utilisateur']);

        // Détermine si le message doit être validé par un modérateur
        $validation_moderation = ($isAdmin || $isModo || $isAffranchi);

        // Ajoute le message à la discussion
        $result = $model->addMessageToDiscussion(
            $texteMessage,
            $discussion['id_utilisateur'],
            $discussion['id_utilisateur_1'],
            $discussionId,
            $validation_moderation,
            $user['id_utilisateur']
        );

        // Redirige vers la discussion après l'envoi du message
        header('Location: ?controller=discussion&action=discussion&id=' . $discussionId);
        exit();
    }


    /**
     * Démarre une nouvelle discussion.
     *
     * Vérifie si la méthode de requête est POST.
     * Vérifie l'accès de l'utilisateur.
     * Récupère l'ID du client à partir de l'utilisateur connecté.
     * Récupère l'ID du formateur à partir des données POST.
     * Démarre une nouvelle discussion entre le client et le formateur.
     * Redirige vers la discussion nouvellement créée.
     */
    public function action_start_discussion()
    {
        // Vérifie si la méthode de requête est POST
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            header('Location: ?controller=discussion');
            exit();
        }

        // Vérifie l'accès de l'utilisateur
        $user = checkUserAccess();
        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
        }

        // Récupère le modèle
        $model = Model::getModel();

        // Récupère l'ID du client à partir de l'utilisateur connecté
        $id_client = $user['id_utilisateur'];

        // Récupère l'ID du formateur à partir des données POST
        $id_formateur = isset($_POST['id_formateur']) ? e($_POST['id_formateur']) : null;

        // Démarre une nouvelle discussion entre le client et le formateur
        $discussion_id = $model->startDiscussion($id_client, $id_formateur);

        // Redirige vers la discussion nouvellement créée
        if (!$discussion_id) {
            header('Location: ?controller=discussion');
            exit();
        }
        header('Location: ?controller=discussion&action=discussion&id=' . $discussion_id);
        exit();
    }


    /**
     * Valide un message dans une discussion.
     *
     * Vérifie l'accès de l'utilisateur.
     * Vérifie si l'utilisateur est un modérateur.
     * Récupère l'ID du message à valider depuis la requête GET.
     * Redirige vers la liste des discussions en cas d'absence d'ID de message.
     * Appelle la fonction du modèle pour valider le message.
     * Redirige vers la discussion après la validation du message.
     * Affiche un message d'erreur en cas d'échec de la validation du message.
     */
    public function action_validate_message()
    {
        // Vérifie l'accès de l'utilisateur
        $user = checkUserAccess();
        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
        }

        // Récupère le modèle
        $model = Model::getModel();

        // Vérifie si l'utilisateur est un modérateur
        $isModo = $model->verifModerateur($user['id_utilisateur']);
        if (!$isModo) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
        }

        // Récupère l'ID du message à valider depuis la requête GET
        $id_message = isset($_GET['id_message']) ? e($_GET['id_message']) : null;

        // Redirige vers la liste des discussions en cas d'absence d'ID de message
        if (!$id_message) {
            header('Location: ?controller=discussion');
            exit();
        }

        // Appelle la fonction du modèle pour valider le message
        $discussion_id = $model->validateMessage($id_message);

        // Redirige vers la discussion après la validation du message
        if (!$discussion_id) {
            echo "Erreur lors de la validation du message.";
            exit();
        }
        header('Location: ?controller=discussion&action=discussion&id=' . $discussion_id);
        exit();
    }



}