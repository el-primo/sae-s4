<?php

/**
 * Classe Controller_panel
 * Gère les actions liées au panneau d'administration et de modération.
 */
class Controller_panel extends Controller
{
    /**
     * Action par défaut qui affiche la vue du panneau.
     *
     * @return void
     */
    public function action_default()
    {
        $this->action_panel();
    }

    /**
     * Affiche le panneau d'administration ou de modération en fonction du rôle de l'utilisateur.
     *
     * @return void
     */
    public function action_panel()
    {
        $user = checkUserAccess();

        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        }

        $role = getUserRole($user);
        $model = Model::getModel();

        $isAdmin = $model->verifAdmin($user['id_utilisateur']);
        $isModo = $model->verifModerateur($user['id_utilisateur']);

        $data = [
            'nom' => $user['nom'],
            'prenom' => $user['prenom'],
            'photo_de_profil' => $user['photo_de_profil'],
            'role' => $role
        ];

        if (!$isAdmin && !$isModo) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        } elseif ($isModo) {
            // Liste des discussions et utilisateurs pour le modérateur
            $data['discussions'] = $model->recupererToutesDiscussions();
            $data['utilisateurs'] = $model->recupererUtilisateursNonAffranchis();
            $this->render('panel_moderateur', $data);
        } else {
            // Liste des formateurs pour l'administrateur
            $data['formateurs'] = $model->listeFormateursAvecStatutModerateur();
            $this->render('panel_administrateur', $data);
        }
    }

    /**
     * Gère la promotion et la rétrogradation des modérateurs.
     *
     * @return void
     */
    public function action_manage_moderator()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            header('Location: ?controller=panel');
            exit();
        }

        $user = checkUserAccess();

        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        }

        $role = getUserRole($user);
        $model = Model::getModel();

        $formateurId = isset($_GET['id']) ? e($_GET['id']) : null;
        if (!$formateurId) {
            header('Location: ?controller=panel');
            exit();
        }

        $isAdmin = $model->verifAdmin($user['id_utilisateur']);

        if (!$isAdmin) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        }

        $manage = isset($_GET['manage']) ? strtolower(trim(e($_GET['manage']))) : '';

        if ($manage === 'promote') {
            $model->addModerator($user['id_utilisateur'], $formateurId);
        } elseif ($manage === 'demote') {
            $model->removeModerator($formateurId);
        } else {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        }

        header('Location: ?controller=panel');
        exit();
    }

    /**
     * Ajoute une nouvelle activité.
     *
     * @return void
     */
    public function action_add_activity()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            header('Location: ?controller=discussion');
            exit();
        }

        $user = checkUserAccess();

        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        }

        $role = getUserRole($user);
        $model = Model::getModel();

        $isAdmin = $model->verifAdmin($user['id_utilisateur']);

        if (!$isAdmin) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        }

        // Récupérer les données de la requête POST
        $nom_activite = isset($_POST['name']) ? e($_POST['name']) : null;
        $description = isset($_POST['description']) ? e($_POST['description']) : null;

        // Gestion du téléversement de l'image
        $image = null;
        if (isset($_FILES['photo']) && $_FILES['photo']['error'] === UPLOAD_ERR_OK) {
            $uploadDir = 'Content/data/';
            $uploadPath = $uploadDir . basename($_FILES['photo']['name']);

            // Assurer que le répertoire 'data/img/' existe
            if (!is_dir($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }

            // Déplacer le fichier téléversé vers le répertoire spécifié
            if (move_uploaded_file($_FILES['photo']['tmp_name'], $uploadPath)) {
                $image = $_FILES['photo']['name'];
            } else {
                echo "Échec du téléversement de l'image.";
                exit();
            }
        }

        // Appeler la méthode addActivity avec les données récupérées
        $model->addActivity($nom_activite, $image, $description, $user['id_utilisateur']);

        header('Location: ?controller=panel');
        exit();
    }

    /**
     * Affranchit un utilisateur.
     *
     * @return void
     */
    public function action_add_affranchi()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            header('Location: ?controller=panel');
            exit();
        }

        $user = checkUserAccess();

        if (!$user) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        }

        $role = getUserRole($user);
        $model = Model::getModel();

        $userId = isset($_GET['id']) ? e($_GET['id']) : null;
        if (!$userId) {
            header('Location: ?controller=panel');
            exit();
        }

        $isModo = $model->verifModerateur($user['id_utilisateur']);

        if (!$isModo) {
            echo "Accès non autorisé.";
            $this->render('auth', []);
            return;
        }

        $model->affranchirUtilisateur($user['id_utilisateur'], $userId);

        header('Location: ?controller=panel');
        exit();
    }
}
?>